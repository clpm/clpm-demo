(uiop:define-package #:clpm-demo
    (:use #:cl
          #:alexandria)
  (:export #:my-iota))

(in-package #:clpm-demo)

(defun my-iota (n)
  (iota n :start 1))
