(uiop:define-package #:clpm-demo-test
    (:use #:cl
          #:clpm-demo
          #:fiveam))

(in-package #:clpm-demo-test)

(def-suite :clpm-demo)
(in-suite :clpm-demo)

(def-test my-iota-test ()
  (is (equal '(1 2 3 4 5) (my-iota 5))))
