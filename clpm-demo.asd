;;;; CLPM-Demo System Definition
;;;;
;;;; This software is part of CLPM. See README.org for more information. See
;;;; LICENSE for license information.

(defsystem #:clpm-demo
  :version "0.1.0"
  :description "CLPM Demo System"
  :license "BSD-2-Clause"
  :depends-on (#:alexandria)
  :in-order-to ((test-op (load-op "clpm-demo-test")))
  :perform (test-op (op c) (unless (symbol-call :fiveam :run! :clpm-demo)
                             (error "Test failure!")))
  :components
  ((:file "demo")))
