;;;; CLPM-Demo System Definition
;;;;
;;;; This software is part of CLPM. See README.org for more information. See
;;;; LICENSE for license information.

(defsystem #:clpm-demo-test
  :version "0.1.0"
  :description "CLPM Demo Test System"
  :license "BSD-2-Clause"
  :depends-on (#:clpm-demo)
  :components
  ((:file "test")))
